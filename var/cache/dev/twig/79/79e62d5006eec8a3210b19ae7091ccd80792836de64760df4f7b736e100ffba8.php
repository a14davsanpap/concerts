<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_cbe7533ba7e4fbb56103997f4660d86ae496e5649f7089b944846bef542c7c66 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0bae5bc6a66f5bcc728485d60a3754a4d856aef99f03b96c072b0976a1d2c7a4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0bae5bc6a66f5bcc728485d60a3754a4d856aef99f03b96c072b0976a1d2c7a4->enter($__internal_0bae5bc6a66f5bcc728485d60a3754a4d856aef99f03b96c072b0976a1d2c7a4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $__internal_5d0ce810bba703bc7f83dc31f1378e11fa6690ebf45a9d39b2a28b1d7730b315 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5d0ce810bba703bc7f83dc31f1378e11fa6690ebf45a9d39b2a28b1d7730b315->enter($__internal_5d0ce810bba703bc7f83dc31f1378e11fa6690ebf45a9d39b2a28b1d7730b315_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0bae5bc6a66f5bcc728485d60a3754a4d856aef99f03b96c072b0976a1d2c7a4->leave($__internal_0bae5bc6a66f5bcc728485d60a3754a4d856aef99f03b96c072b0976a1d2c7a4_prof);

        
        $__internal_5d0ce810bba703bc7f83dc31f1378e11fa6690ebf45a9d39b2a28b1d7730b315->leave($__internal_5d0ce810bba703bc7f83dc31f1378e11fa6690ebf45a9d39b2a28b1d7730b315_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_9b4ff4c499b848d455c4b274fec380c74d889abbe986592f62dacd0b28c7b2a8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9b4ff4c499b848d455c4b274fec380c74d889abbe986592f62dacd0b28c7b2a8->enter($__internal_9b4ff4c499b848d455c4b274fec380c74d889abbe986592f62dacd0b28c7b2a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_b6a19f6a8c856c76252b751b2e4f86fc72156278e3da97e1671edf48f52c419e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b6a19f6a8c856c76252b751b2e4f86fc72156278e3da97e1671edf48f52c419e->enter($__internal_b6a19f6a8c856c76252b751b2e4f86fc72156278e3da97e1671edf48f52c419e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $__internal_b6a19f6a8c856c76252b751b2e4f86fc72156278e3da97e1671edf48f52c419e->leave($__internal_b6a19f6a8c856c76252b751b2e4f86fc72156278e3da97e1671edf48f52c419e_prof);

        
        $__internal_9b4ff4c499b848d455c4b274fec380c74d889abbe986592f62dacd0b28c7b2a8->leave($__internal_9b4ff4c499b848d455c4b274fec380c74d889abbe986592f62dacd0b28c7b2a8_prof);

    }

    // line 136
    public function block_title($context, array $blocks = array())
    {
        $__internal_15e9fbb58759b5e7b65e569a8eb368fe05032b5abb946214f399774345ce5354 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_15e9fbb58759b5e7b65e569a8eb368fe05032b5abb946214f399774345ce5354->enter($__internal_15e9fbb58759b5e7b65e569a8eb368fe05032b5abb946214f399774345ce5354_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_59cc07686d48354a8d71bd72979dde608f672f17f838dcd6f1652effbae82c9b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_59cc07686d48354a8d71bd72979dde608f672f17f838dcd6f1652effbae82c9b->enter($__internal_59cc07686d48354a8d71bd72979dde608f672f17f838dcd6f1652effbae82c9b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_59cc07686d48354a8d71bd72979dde608f672f17f838dcd6f1652effbae82c9b->leave($__internal_59cc07686d48354a8d71bd72979dde608f672f17f838dcd6f1652effbae82c9b_prof);

        
        $__internal_15e9fbb58759b5e7b65e569a8eb368fe05032b5abb946214f399774345ce5354->leave($__internal_15e9fbb58759b5e7b65e569a8eb368fe05032b5abb946214f399774345ce5354_prof);

    }

    // line 140
    public function block_body($context, array $blocks = array())
    {
        $__internal_a2ce5e2283da7724a018adfdabffc44d35dcb7ec56b059d9021d03f441b42547 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a2ce5e2283da7724a018adfdabffc44d35dcb7ec56b059d9021d03f441b42547->enter($__internal_a2ce5e2283da7724a018adfdabffc44d35dcb7ec56b059d9021d03f441b42547_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_ef8f22486555fbb70398ba7df07b7bdf5d53e17d1346d672136a0d736c75c2b1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ef8f22486555fbb70398ba7df07b7bdf5d53e17d1346d672136a0d736c75c2b1->enter($__internal_ef8f22486555fbb70398ba7df07b7bdf5d53e17d1346d672136a0d736c75c2b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 141)->display($context);
        
        $__internal_ef8f22486555fbb70398ba7df07b7bdf5d53e17d1346d672136a0d736c75c2b1->leave($__internal_ef8f22486555fbb70398ba7df07b7bdf5d53e17d1346d672136a0d736c75c2b1_prof);

        
        $__internal_a2ce5e2283da7724a018adfdabffc44d35dcb7ec56b059d9021d03f441b42547->leave($__internal_a2ce5e2283da7724a018adfdabffc44d35dcb7ec56b059d9021d03f441b42547_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "/home/david/Escritorio/test/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}
