<?php

/* concert/content.html.twig */
class __TwigTemplate_8ac24caabf9573d8d74b41fee7ed0cbf04052be146986708acc22610bad76175 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "concert/content.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9202d3108397bc6fadd31b01556400107a2d88777d413d3edd323fba15787996 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9202d3108397bc6fadd31b01556400107a2d88777d413d3edd323fba15787996->enter($__internal_9202d3108397bc6fadd31b01556400107a2d88777d413d3edd323fba15787996_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "concert/content.html.twig"));

        $__internal_8956af0d5c6b6fb610ce7cd7417e9a629ad0cfa28d14cc216eca14c230fe51fd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8956af0d5c6b6fb610ce7cd7417e9a629ad0cfa28d14cc216eca14c230fe51fd->enter($__internal_8956af0d5c6b6fb610ce7cd7417e9a629ad0cfa28d14cc216eca14c230fe51fd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "concert/content.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9202d3108397bc6fadd31b01556400107a2d88777d413d3edd323fba15787996->leave($__internal_9202d3108397bc6fadd31b01556400107a2d88777d413d3edd323fba15787996_prof);

        
        $__internal_8956af0d5c6b6fb610ce7cd7417e9a629ad0cfa28d14cc216eca14c230fe51fd->leave($__internal_8956af0d5c6b6fb610ce7cd7417e9a629ad0cfa28d14cc216eca14c230fe51fd_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_cd8748c27e839ec83cdcf32ed42f90cb0b48a651c9ecada13778bfe9e74257b5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cd8748c27e839ec83cdcf32ed42f90cb0b48a651c9ecada13778bfe9e74257b5->enter($__internal_cd8748c27e839ec83cdcf32ed42f90cb0b48a651c9ecada13778bfe9e74257b5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_37c4576620b5d08871ed292ab13565d1bb2d68f019e54499541b282ebe460e9c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_37c4576620b5d08871ed292ab13565d1bb2d68f019e54499541b282ebe460e9c->enter($__internal_37c4576620b5d08871ed292ab13565d1bb2d68f019e54499541b282ebe460e9c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h3> Llistar de Concerts </h3>
<table border=\"1\">
    <tr>
        <th>Codi</th>
        <th>Nom</th>
        <th>Autor</th>
        <th>Nom del grup</th>
        <th>Data</th>
        <th>Ciutat</th>
        <th>Espai</th>
    </tr>
    ";
        // line 15
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["concert"]);
        foreach ($context['_seq'] as $context["_key"] => $context["concert"]) {
            // line 16
            echo "        <tr>
            <td>";
            // line 17
            echo twig_escape_filter($this->env, $this->getAttribute($context["concert"], "codi", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute($context["concert"], "nom", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($context["concert"], "autor", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 20
            echo twig_escape_filter($this->env, $this->getAttribute($context["concert"], "nomgrup", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute($context["concert"], "data", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($context["concert"], "ciutat", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($context["concert"], "espai", array()), "html", null, true);
            echo "</td>
        </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['concert'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "</table>
";
        
        $__internal_37c4576620b5d08871ed292ab13565d1bb2d68f019e54499541b282ebe460e9c->leave($__internal_37c4576620b5d08871ed292ab13565d1bb2d68f019e54499541b282ebe460e9c_prof);

        
        $__internal_cd8748c27e839ec83cdcf32ed42f90cb0b48a651c9ecada13778bfe9e74257b5->leave($__internal_cd8748c27e839ec83cdcf32ed42f90cb0b48a651c9ecada13778bfe9e74257b5_prof);

    }

    public function getTemplateName()
    {
        return "concert/content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 26,  93 => 23,  89 => 22,  85 => 21,  81 => 20,  77 => 19,  73 => 18,  69 => 17,  66 => 16,  62 => 15,  49 => 4,  40 => 3,  11 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# app/Resources/views/customer/content.html.twig #}
{% extends 'base.html.twig' %}
{% block body %}
    <h3> Llistar de Concerts </h3>
<table border=\"1\">
    <tr>
        <th>Codi</th>
        <th>Nom</th>
        <th>Autor</th>
        <th>Nom del grup</th>
        <th>Data</th>
        <th>Ciutat</th>
        <th>Espai</th>
    </tr>
    {% for concert in concert %}
        <tr>
            <td>{{ concert.codi }}</td>
            <td>{{ concert.nom }}</td>
            <td>{{ concert.autor }}</td>
            <td>{{ concert.nomgrup }}</td>
            <td>{{ concert.data }}</td>
            <td>{{ concert.ciutat }}</td>
            <td>{{ concert.espai }}</td>
        </tr>
    {% endfor %}
</table>
{% endblock %}
", "concert/content.html.twig", "/home/david/Escritorio/test/app/Resources/views/concert/content.html.twig");
    }
}
