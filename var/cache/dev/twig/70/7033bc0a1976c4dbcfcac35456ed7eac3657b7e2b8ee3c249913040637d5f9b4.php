<?php

/* default/message.html.twig */
class __TwigTemplate_67deb92e0a0abacc99f7c6fd2d8a03dd01be5c3cf41c244a686f118681358cf8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "default/message.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_668ca006c4052239240cffdaa92d9c24dfe044396c02ce87e467092cdec90a2c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_668ca006c4052239240cffdaa92d9c24dfe044396c02ce87e467092cdec90a2c->enter($__internal_668ca006c4052239240cffdaa92d9c24dfe044396c02ce87e467092cdec90a2c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/message.html.twig"));

        $__internal_da8d89c8caf03a1426c9e79b688516e84280ebde67c810408a98ab13a1812502 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_da8d89c8caf03a1426c9e79b688516e84280ebde67c810408a98ab13a1812502->enter($__internal_da8d89c8caf03a1426c9e79b688516e84280ebde67c810408a98ab13a1812502_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/message.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_668ca006c4052239240cffdaa92d9c24dfe044396c02ce87e467092cdec90a2c->leave($__internal_668ca006c4052239240cffdaa92d9c24dfe044396c02ce87e467092cdec90a2c_prof);

        
        $__internal_da8d89c8caf03a1426c9e79b688516e84280ebde67c810408a98ab13a1812502->leave($__internal_da8d89c8caf03a1426c9e79b688516e84280ebde67c810408a98ab13a1812502_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_3de69746a192ad57cda747bf49026f0be046fcb2a7605eee9b94d59430c37612 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3de69746a192ad57cda747bf49026f0be046fcb2a7605eee9b94d59430c37612->enter($__internal_3de69746a192ad57cda747bf49026f0be046fcb2a7605eee9b94d59430c37612_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_c99e81b3b71402d8d4677563fa3f1160f7e28f6e0cc42789c775ebc23815784d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c99e81b3b71402d8d4677563fa3f1160f7e28f6e0cc42789c775ebc23815784d->enter($__internal_c99e81b3b71402d8d4677563fa3f1160f7e28f6e0cc42789c775ebc23815784d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    ";
        echo twig_escape_filter($this->env, ($context["message"] ?? $this->getContext($context, "message")), "html", null, true);
        echo "
";
        
        $__internal_c99e81b3b71402d8d4677563fa3f1160f7e28f6e0cc42789c775ebc23815784d->leave($__internal_c99e81b3b71402d8d4677563fa3f1160f7e28f6e0cc42789c775ebc23815784d_prof);

        
        $__internal_3de69746a192ad57cda747bf49026f0be046fcb2a7605eee9b94d59430c37612->leave($__internal_3de69746a192ad57cda747bf49026f0be046fcb2a7605eee9b94d59430c37612_prof);

    }

    public function getTemplateName()
    {
        return "default/message.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# app/Resources/views/default/message.html.twig #}
{% extends 'base.html.twig' %}
{% block body %}
    {{ message }}
{% endblock %}





", "default/message.html.twig", "/home/david/Escritorio/test/app/Resources/views/default/message.html.twig");
    }
}
