<?php

/* @Twig/layout.html.twig */
class __TwigTemplate_21e1384dbb5ce9486a9d17503df7b90e48bc1b6ea63609664f02070f7deb3d3f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_86f0adaa7d96e536723f53f0f6fdf8a765d9164c4bb9ce6a3b69da32050758b3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_86f0adaa7d96e536723f53f0f6fdf8a765d9164c4bb9ce6a3b69da32050758b3->enter($__internal_86f0adaa7d96e536723f53f0f6fdf8a765d9164c4bb9ce6a3b69da32050758b3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        $__internal_47b42d1719cc26bf28af79a999a8f0649b6916092ada4dd879e5609e68982304 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_47b42d1719cc26bf28af79a999a8f0649b6916092ada4dd879e5609e68982304->enter($__internal_47b42d1719cc26bf28af79a999a8f0649b6916092ada4dd879e5609e68982304_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 8
        echo twig_include($this->env, $context, "@Twig/images/favicon.png.base64");
        echo "\">
        <style>";
        // line 9
        echo twig_include($this->env, $context, "@Twig/exception.css.twig");
        echo "</style>
        ";
        // line 10
        $this->displayBlock('head', $context, $blocks);
        // line 11
        echo "    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">";
        // line 15
        echo twig_include($this->env, $context, "@Twig/images/symfony-logo.svg");
        echo " Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">";
        // line 19
        echo twig_include($this->env, $context, "@Twig/images/icon-book.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">";
        // line 26
        echo twig_include($this->env, $context, "@Twig/images/icon-support.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        ";
        // line 33
        $this->displayBlock('body', $context, $blocks);
        // line 34
        echo "        ";
        echo twig_include($this->env, $context, "@Twig/base_js.html.twig");
        echo "
    </body>
</html>
";
        
        $__internal_86f0adaa7d96e536723f53f0f6fdf8a765d9164c4bb9ce6a3b69da32050758b3->leave($__internal_86f0adaa7d96e536723f53f0f6fdf8a765d9164c4bb9ce6a3b69da32050758b3_prof);

        
        $__internal_47b42d1719cc26bf28af79a999a8f0649b6916092ada4dd879e5609e68982304->leave($__internal_47b42d1719cc26bf28af79a999a8f0649b6916092ada4dd879e5609e68982304_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_e35ebd4cef5b8b3418307cd3f703bbbd90c521a69bec9f4a44050e11f1465a6f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e35ebd4cef5b8b3418307cd3f703bbbd90c521a69bec9f4a44050e11f1465a6f->enter($__internal_e35ebd4cef5b8b3418307cd3f703bbbd90c521a69bec9f4a44050e11f1465a6f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_8053af0c6f126f3a65ab15fbee018d1cc35203120b48d45d508b5816652deb28 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8053af0c6f126f3a65ab15fbee018d1cc35203120b48d45d508b5816652deb28->enter($__internal_8053af0c6f126f3a65ab15fbee018d1cc35203120b48d45d508b5816652deb28_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_8053af0c6f126f3a65ab15fbee018d1cc35203120b48d45d508b5816652deb28->leave($__internal_8053af0c6f126f3a65ab15fbee018d1cc35203120b48d45d508b5816652deb28_prof);

        
        $__internal_e35ebd4cef5b8b3418307cd3f703bbbd90c521a69bec9f4a44050e11f1465a6f->leave($__internal_e35ebd4cef5b8b3418307cd3f703bbbd90c521a69bec9f4a44050e11f1465a6f_prof);

    }

    // line 10
    public function block_head($context, array $blocks = array())
    {
        $__internal_d8eb850221375eac9d2d264b7b203b0eb008893cc1731b9362adf6980cdd68bb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d8eb850221375eac9d2d264b7b203b0eb008893cc1731b9362adf6980cdd68bb->enter($__internal_d8eb850221375eac9d2d264b7b203b0eb008893cc1731b9362adf6980cdd68bb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_a3fe3cd42d96029818d0ef94f85fd227e18426e5eb8b981ea7481cda01ddcebe = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a3fe3cd42d96029818d0ef94f85fd227e18426e5eb8b981ea7481cda01ddcebe->enter($__internal_a3fe3cd42d96029818d0ef94f85fd227e18426e5eb8b981ea7481cda01ddcebe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        
        $__internal_a3fe3cd42d96029818d0ef94f85fd227e18426e5eb8b981ea7481cda01ddcebe->leave($__internal_a3fe3cd42d96029818d0ef94f85fd227e18426e5eb8b981ea7481cda01ddcebe_prof);

        
        $__internal_d8eb850221375eac9d2d264b7b203b0eb008893cc1731b9362adf6980cdd68bb->leave($__internal_d8eb850221375eac9d2d264b7b203b0eb008893cc1731b9362adf6980cdd68bb_prof);

    }

    // line 33
    public function block_body($context, array $blocks = array())
    {
        $__internal_b0bc5fcc148f7df34c3947dafa752ddae8161f7632288a02487d8428c126ee66 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b0bc5fcc148f7df34c3947dafa752ddae8161f7632288a02487d8428c126ee66->enter($__internal_b0bc5fcc148f7df34c3947dafa752ddae8161f7632288a02487d8428c126ee66_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_08c2cbd78ea405db9e6a4462d6bd7d03576324751988a2ab483ad14c73a8b185 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_08c2cbd78ea405db9e6a4462d6bd7d03576324751988a2ab483ad14c73a8b185->enter($__internal_08c2cbd78ea405db9e6a4462d6bd7d03576324751988a2ab483ad14c73a8b185_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_08c2cbd78ea405db9e6a4462d6bd7d03576324751988a2ab483ad14c73a8b185->leave($__internal_08c2cbd78ea405db9e6a4462d6bd7d03576324751988a2ab483ad14c73a8b185_prof);

        
        $__internal_b0bc5fcc148f7df34c3947dafa752ddae8161f7632288a02487d8428c126ee66->leave($__internal_b0bc5fcc148f7df34c3947dafa752ddae8161f7632288a02487d8428c126ee66_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 33,  120 => 10,  103 => 7,  88 => 34,  86 => 33,  76 => 26,  66 => 19,  59 => 15,  53 => 11,  51 => 10,  47 => 9,  43 => 8,  39 => 7,  33 => 4,  28 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"{{ _charset }}\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>{% block title %}{% endblock %}</title>
        <link rel=\"icon\" type=\"image/png\" href=\"{{ include('@Twig/images/favicon.png.base64') }}\">
        <style>{{ include('@Twig/exception.css.twig') }}</style>
        {% block head %}{% endblock %}
    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">{{ include('@Twig/images/symfony-logo.svg') }} Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-book.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-support.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        {% block body %}{% endblock %}
        {{ include('@Twig/base_js.html.twig') }}
    </body>
</html>
", "@Twig/layout.html.twig", "/home/david/Escritorio/test/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/layout.html.twig");
    }
}
