<?php

/* customer/content.html.twig */
class __TwigTemplate_5877649a0a7e01b6fcb44126e8cc3b7a77822a0bc52aef04dacfead0ae9da18d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "customer/content.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2daca74fa8935ad58d50ecfacac667c0cbf74fba58b9136a56f99f154f8ff1f7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2daca74fa8935ad58d50ecfacac667c0cbf74fba58b9136a56f99f154f8ff1f7->enter($__internal_2daca74fa8935ad58d50ecfacac667c0cbf74fba58b9136a56f99f154f8ff1f7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "customer/content.html.twig"));

        $__internal_96b110344cc7c46a338741377a7c2239340a024fd07d0764236e06975b92200f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_96b110344cc7c46a338741377a7c2239340a024fd07d0764236e06975b92200f->enter($__internal_96b110344cc7c46a338741377a7c2239340a024fd07d0764236e06975b92200f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "customer/content.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2daca74fa8935ad58d50ecfacac667c0cbf74fba58b9136a56f99f154f8ff1f7->leave($__internal_2daca74fa8935ad58d50ecfacac667c0cbf74fba58b9136a56f99f154f8ff1f7_prof);

        
        $__internal_96b110344cc7c46a338741377a7c2239340a024fd07d0764236e06975b92200f->leave($__internal_96b110344cc7c46a338741377a7c2239340a024fd07d0764236e06975b92200f_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_31085393158cdd84a68deaf007adc943172b7b057cab6ea0685e5d75cacc4bc5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_31085393158cdd84a68deaf007adc943172b7b057cab6ea0685e5d75cacc4bc5->enter($__internal_31085393158cdd84a68deaf007adc943172b7b057cab6ea0685e5d75cacc4bc5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_bd661c22be5ba6aae380bc27fd8d465dabcb4b17d8b916553a0599b5722f50e0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bd661c22be5ba6aae380bc27fd8d465dabcb4b17d8b916553a0599b5722f50e0->enter($__internal_bd661c22be5ba6aae380bc27fd8d465dabcb4b17d8b916553a0599b5722f50e0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h3> LLista de Concerts </h3>
<table border=\"1\">
    <tr>
        <th>Codi</th>
        <th>Nom</th>
        <th>Autor</th>
        <th>Nom del grup</th>
        <th>Data</th>
        <th>Ciutat</th>
        <th>Espai</th>
    </tr>
    ";
        // line 15
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["concert"]);
        foreach ($context['_seq'] as $context["_key"] => $context["concert"]) {
            // line 16
            echo "        <tr>
            <td>";
            // line 17
            echo twig_escape_filter($this->env, $this->getAttribute($context["concert"], "codi", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute($context["concert"], "nom", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($context["concert"], "autor", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 20
            echo twig_escape_filter($this->env, $this->getAttribute($context["concert"], "nomgrup", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute($context["concert"], "data", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($context["concert"], "ciutat", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($context["concert"], "espai", array()), "html", null, true);
            echo "</td>
        </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['concert'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "</table>
";
        
        $__internal_bd661c22be5ba6aae380bc27fd8d465dabcb4b17d8b916553a0599b5722f50e0->leave($__internal_bd661c22be5ba6aae380bc27fd8d465dabcb4b17d8b916553a0599b5722f50e0_prof);

        
        $__internal_31085393158cdd84a68deaf007adc943172b7b057cab6ea0685e5d75cacc4bc5->leave($__internal_31085393158cdd84a68deaf007adc943172b7b057cab6ea0685e5d75cacc4bc5_prof);

    }

    public function getTemplateName()
    {
        return "customer/content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 26,  93 => 23,  89 => 22,  85 => 21,  81 => 20,  77 => 19,  73 => 18,  69 => 17,  66 => 16,  62 => 15,  49 => 4,  40 => 3,  11 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# app/Resources/views/customer/content.html.twig #}
{% extends 'base.html.twig' %}
{% block body %}
    <h3> LLista de Concerts </h3>
<table border=\"1\">
    <tr>
        <th>Codi</th>
        <th>Nom</th>
        <th>Autor</th>
        <th>Nom del grup</th>
        <th>Data</th>
        <th>Ciutat</th>
        <th>Espai</th>
    </tr>
    {% for concert in concert %}
        <tr>
            <td>{{ concert.codi }}</td>
            <td>{{ concert.nom }}</td>
            <td>{{ concert.autor }}</td>
            <td>{{ concert.nomgrup }}</td>
            <td>{{ concert.data }}</td>
            <td>{{ concert.ciutat }}</td>
            <td>{{ concert.espai }}</td>
        </tr>
    {% endfor %}
</table>
{% endblock %}
", "customer/content.html.twig", "/home/david/Escritorio/test/app/Resources/views/customer/content.html.twig");
    }
}
