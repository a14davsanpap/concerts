<?php

/* base.html.twig */
class __TwigTemplate_2e55ceb3e3a8172ee2bdb98420a739c71281e5768b9082c463522045b60e4555 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9048573a485b8ef0369fa444a8ef378bc9655ed7fc1d0433c3da9354c49c0b6c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9048573a485b8ef0369fa444a8ef378bc9655ed7fc1d0433c3da9354c49c0b6c->enter($__internal_9048573a485b8ef0369fa444a8ef378bc9655ed7fc1d0433c3da9354c49c0b6c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_2dd3e948fdd23a0a6aac0b21c90a6c12fa435d0f4bf5869e2b7d20df5efa5350 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2dd3e948fdd23a0a6aac0b21c90a6c12fa435d0f4bf5869e2b7d20df5efa5350->enter($__internal_2dd3e948fdd23a0a6aac0b21c90a6c12fa435d0f4bf5869e2b7d20df5efa5350_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_9048573a485b8ef0369fa444a8ef378bc9655ed7fc1d0433c3da9354c49c0b6c->leave($__internal_9048573a485b8ef0369fa444a8ef378bc9655ed7fc1d0433c3da9354c49c0b6c_prof);

        
        $__internal_2dd3e948fdd23a0a6aac0b21c90a6c12fa435d0f4bf5869e2b7d20df5efa5350->leave($__internal_2dd3e948fdd23a0a6aac0b21c90a6c12fa435d0f4bf5869e2b7d20df5efa5350_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_1071dda229eb0f55844b74a00f76da682677d2ad89d3f39438824d946a04d0c8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1071dda229eb0f55844b74a00f76da682677d2ad89d3f39438824d946a04d0c8->enter($__internal_1071dda229eb0f55844b74a00f76da682677d2ad89d3f39438824d946a04d0c8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_1b2399f53ffc2454699e2f8441512a627f9cfe3cbc707e75b006115167894951 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1b2399f53ffc2454699e2f8441512a627f9cfe3cbc707e75b006115167894951->enter($__internal_1b2399f53ffc2454699e2f8441512a627f9cfe3cbc707e75b006115167894951_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_1b2399f53ffc2454699e2f8441512a627f9cfe3cbc707e75b006115167894951->leave($__internal_1b2399f53ffc2454699e2f8441512a627f9cfe3cbc707e75b006115167894951_prof);

        
        $__internal_1071dda229eb0f55844b74a00f76da682677d2ad89d3f39438824d946a04d0c8->leave($__internal_1071dda229eb0f55844b74a00f76da682677d2ad89d3f39438824d946a04d0c8_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_d3432efe3c153e12e39e6f2101716a0d887901817cc232d0e80d77ec19ee3998 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d3432efe3c153e12e39e6f2101716a0d887901817cc232d0e80d77ec19ee3998->enter($__internal_d3432efe3c153e12e39e6f2101716a0d887901817cc232d0e80d77ec19ee3998_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_ce7a0bbdba53b803ae3f907f32dc50940f689790377c1c99ebc2e0cdcf4dcb28 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ce7a0bbdba53b803ae3f907f32dc50940f689790377c1c99ebc2e0cdcf4dcb28->enter($__internal_ce7a0bbdba53b803ae3f907f32dc50940f689790377c1c99ebc2e0cdcf4dcb28_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_ce7a0bbdba53b803ae3f907f32dc50940f689790377c1c99ebc2e0cdcf4dcb28->leave($__internal_ce7a0bbdba53b803ae3f907f32dc50940f689790377c1c99ebc2e0cdcf4dcb28_prof);

        
        $__internal_d3432efe3c153e12e39e6f2101716a0d887901817cc232d0e80d77ec19ee3998->leave($__internal_d3432efe3c153e12e39e6f2101716a0d887901817cc232d0e80d77ec19ee3998_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_4338f383124bf288ce0dea7e39ff43272ab1fc6c445657fa07c2aada4abb9e0f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4338f383124bf288ce0dea7e39ff43272ab1fc6c445657fa07c2aada4abb9e0f->enter($__internal_4338f383124bf288ce0dea7e39ff43272ab1fc6c445657fa07c2aada4abb9e0f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_7f58af487de5b18c8620aa731b2e501c675b0d8e382e54e3e0796fae1e543b09 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7f58af487de5b18c8620aa731b2e501c675b0d8e382e54e3e0796fae1e543b09->enter($__internal_7f58af487de5b18c8620aa731b2e501c675b0d8e382e54e3e0796fae1e543b09_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_7f58af487de5b18c8620aa731b2e501c675b0d8e382e54e3e0796fae1e543b09->leave($__internal_7f58af487de5b18c8620aa731b2e501c675b0d8e382e54e3e0796fae1e543b09_prof);

        
        $__internal_4338f383124bf288ce0dea7e39ff43272ab1fc6c445657fa07c2aada4abb9e0f->leave($__internal_4338f383124bf288ce0dea7e39ff43272ab1fc6c445657fa07c2aada4abb9e0f_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_5d04c74985438df5a9b7e65a66ee56f3300a2b82c6c3c3438f78a598dd8dd0e9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5d04c74985438df5a9b7e65a66ee56f3300a2b82c6c3c3438f78a598dd8dd0e9->enter($__internal_5d04c74985438df5a9b7e65a66ee56f3300a2b82c6c3c3438f78a598dd8dd0e9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_473f894bf4bbdf90995401e864d15abbf8d4ef96148129e0e1cf596fe67358de = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_473f894bf4bbdf90995401e864d15abbf8d4ef96148129e0e1cf596fe67358de->enter($__internal_473f894bf4bbdf90995401e864d15abbf8d4ef96148129e0e1cf596fe67358de_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_473f894bf4bbdf90995401e864d15abbf8d4ef96148129e0e1cf596fe67358de->leave($__internal_473f894bf4bbdf90995401e864d15abbf8d4ef96148129e0e1cf596fe67358de_prof);

        
        $__internal_5d04c74985438df5a9b7e65a66ee56f3300a2b82c6c3c3438f78a598dd8dd0e9->leave($__internal_5d04c74985438df5a9b7e65a66ee56f3300a2b82c6c3c3438f78a598dd8dd0e9_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 11,  100 => 10,  83 => 6,  65 => 5,  53 => 12,  50 => 11,  48 => 10,  41 => 7,  39 => 6,  35 => 5,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "base.html.twig", "/home/david/Escritorio/test/app/Resources/views/base.html.twig");
    }
}
