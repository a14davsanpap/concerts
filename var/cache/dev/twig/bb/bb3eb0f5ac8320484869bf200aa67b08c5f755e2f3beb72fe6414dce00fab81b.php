<?php

/* default/index.html.twig */
class __TwigTemplate_3f9ca622c58d6fb11b92666a1b61068f43c9897b184a822182047e91f7de3519 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "default/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f6ef5fd6095f377297a5a86963b80741a2d1848f986f979078e1fcb6c2af797f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f6ef5fd6095f377297a5a86963b80741a2d1848f986f979078e1fcb6c2af797f->enter($__internal_f6ef5fd6095f377297a5a86963b80741a2d1848f986f979078e1fcb6c2af797f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/index.html.twig"));

        $__internal_c19780d3f0f66c75aa84b04fd5283848150ca92465b69100d603da793a108701 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c19780d3f0f66c75aa84b04fd5283848150ca92465b69100d603da793a108701->enter($__internal_c19780d3f0f66c75aa84b04fd5283848150ca92465b69100d603da793a108701_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f6ef5fd6095f377297a5a86963b80741a2d1848f986f979078e1fcb6c2af797f->leave($__internal_f6ef5fd6095f377297a5a86963b80741a2d1848f986f979078e1fcb6c2af797f_prof);

        
        $__internal_c19780d3f0f66c75aa84b04fd5283848150ca92465b69100d603da793a108701->leave($__internal_c19780d3f0f66c75aa84b04fd5283848150ca92465b69100d603da793a108701_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_246fd95082d25bd9621d1b6f0d2cfbd4e3489d0ea5f784835145b68a780c3293 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_246fd95082d25bd9621d1b6f0d2cfbd4e3489d0ea5f784835145b68a780c3293->enter($__internal_246fd95082d25bd9621d1b6f0d2cfbd4e3489d0ea5f784835145b68a780c3293_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_3752005c7a01d21d5bccfe2539ecead5a1f84d932fe591267f8b1d80c8e028c1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3752005c7a01d21d5bccfe2539ecead5a1f84d932fe591267f8b1d80c8e028c1->enter($__internal_3752005c7a01d21d5bccfe2539ecead5a1f84d932fe591267f8b1d80c8e028c1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<h1>CONCERTS</h1>
<a href=\"";
        // line 5
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getUrl("selectConcerts");
        echo "\">Llistar</a>
<a href=\"";
        // line 6
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getUrl("selecionarConcert");
        echo "\">Cerca</a>
<a href=\"";
        // line 7
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getUrl("insertConcert");
        echo "\">Inserir</a>
<a href=\"";
        // line 8
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getUrl("removeConcert");
        echo "\">Eliminar</a>
";
        
        $__internal_3752005c7a01d21d5bccfe2539ecead5a1f84d932fe591267f8b1d80c8e028c1->leave($__internal_3752005c7a01d21d5bccfe2539ecead5a1f84d932fe591267f8b1d80c8e028c1_prof);

        
        $__internal_246fd95082d25bd9621d1b6f0d2cfbd4e3489d0ea5f784835145b68a780c3293->leave($__internal_246fd95082d25bd9621d1b6f0d2cfbd4e3489d0ea5f784835145b68a780c3293_prof);

    }

    public function getTemplateName()
    {
        return "default/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 8,  60 => 7,  56 => 6,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
<h1>CONCERTS</h1>
<a href=\"{{ url('selectConcerts') }}\">Llistar</a>
<a href=\"{{ url('selecionarConcert') }}\">Cerca</a>
<a href=\"{{ url('insertConcert') }}\">Inserir</a>
<a href=\"{{ url('removeConcert') }}\">Eliminar</a>
{% endblock %}
", "default/index.html.twig", "/home/david/Escritorio/test/app/Resources/views/default/index.html.twig");
    }
}
