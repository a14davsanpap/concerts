<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_a45254030d9cc98b662ca4ab41f1a18fca407871221561dd5b3a8f7c6d58d3e3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_688e1e36cccd7ad5e352659240b5610e2b09a683f27db7615d2306df7b1b95d6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_688e1e36cccd7ad5e352659240b5610e2b09a683f27db7615d2306df7b1b95d6->enter($__internal_688e1e36cccd7ad5e352659240b5610e2b09a683f27db7615d2306df7b1b95d6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_a04d2a59b8711abe45d01d159905031c0e93bf98d84d23b3d3eb41082ef94196 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a04d2a59b8711abe45d01d159905031c0e93bf98d84d23b3d3eb41082ef94196->enter($__internal_a04d2a59b8711abe45d01d159905031c0e93bf98d84d23b3d3eb41082ef94196_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_688e1e36cccd7ad5e352659240b5610e2b09a683f27db7615d2306df7b1b95d6->leave($__internal_688e1e36cccd7ad5e352659240b5610e2b09a683f27db7615d2306df7b1b95d6_prof);

        
        $__internal_a04d2a59b8711abe45d01d159905031c0e93bf98d84d23b3d3eb41082ef94196->leave($__internal_a04d2a59b8711abe45d01d159905031c0e93bf98d84d23b3d3eb41082ef94196_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_0b55c9cb49c8cf26aaa00e493d4b1200e38c33ddf64337b949df3309213ef403 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0b55c9cb49c8cf26aaa00e493d4b1200e38c33ddf64337b949df3309213ef403->enter($__internal_0b55c9cb49c8cf26aaa00e493d4b1200e38c33ddf64337b949df3309213ef403_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_066900d0ce8a6afffa0f3c99bda3cf83412831d24422f2296b0e8e68552959fd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_066900d0ce8a6afffa0f3c99bda3cf83412831d24422f2296b0e8e68552959fd->enter($__internal_066900d0ce8a6afffa0f3c99bda3cf83412831d24422f2296b0e8e68552959fd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_066900d0ce8a6afffa0f3c99bda3cf83412831d24422f2296b0e8e68552959fd->leave($__internal_066900d0ce8a6afffa0f3c99bda3cf83412831d24422f2296b0e8e68552959fd_prof);

        
        $__internal_0b55c9cb49c8cf26aaa00e493d4b1200e38c33ddf64337b949df3309213ef403->leave($__internal_0b55c9cb49c8cf26aaa00e493d4b1200e38c33ddf64337b949df3309213ef403_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_50bcf6f656a93916f7c6ed841753339a73506e95bc087b78af14d959fdbcb2fb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_50bcf6f656a93916f7c6ed841753339a73506e95bc087b78af14d959fdbcb2fb->enter($__internal_50bcf6f656a93916f7c6ed841753339a73506e95bc087b78af14d959fdbcb2fb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_ae48e7b9447b132b77f0a89eede91053e284686834f8cdd9e6b9db88c419f2c3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ae48e7b9447b132b77f0a89eede91053e284686834f8cdd9e6b9db88c419f2c3->enter($__internal_ae48e7b9447b132b77f0a89eede91053e284686834f8cdd9e6b9db88c419f2c3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_ae48e7b9447b132b77f0a89eede91053e284686834f8cdd9e6b9db88c419f2c3->leave($__internal_ae48e7b9447b132b77f0a89eede91053e284686834f8cdd9e6b9db88c419f2c3_prof);

        
        $__internal_50bcf6f656a93916f7c6ed841753339a73506e95bc087b78af14d959fdbcb2fb->leave($__internal_50bcf6f656a93916f7c6ed841753339a73506e95bc087b78af14d959fdbcb2fb_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_0f5d7bf62a0c1a850c1e83c466f3bab8ebcdf038f7d6557fdf65086cda1f42c0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0f5d7bf62a0c1a850c1e83c466f3bab8ebcdf038f7d6557fdf65086cda1f42c0->enter($__internal_0f5d7bf62a0c1a850c1e83c466f3bab8ebcdf038f7d6557fdf65086cda1f42c0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_2eb25d7773fc7d5a51c89f54f064ecf6c621875e1bfdd4642341f231728edf73 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2eb25d7773fc7d5a51c89f54f064ecf6c621875e1bfdd4642341f231728edf73->enter($__internal_2eb25d7773fc7d5a51c89f54f064ecf6c621875e1bfdd4642341f231728edf73_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_2eb25d7773fc7d5a51c89f54f064ecf6c621875e1bfdd4642341f231728edf73->leave($__internal_2eb25d7773fc7d5a51c89f54f064ecf6c621875e1bfdd4642341f231728edf73_prof);

        
        $__internal_0f5d7bf62a0c1a850c1e83c466f3bab8ebcdf038f7d6557fdf65086cda1f42c0->leave($__internal_0f5d7bf62a0c1a850c1e83c466f3bab8ebcdf038f7d6557fdf65086cda1f42c0_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "/home/david/Escritorio/test/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
