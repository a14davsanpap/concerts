<?php

/* @Twig/images/icon-minus-square.svg */
class __TwigTemplate_e20aec573716b3f112d21a8adf55dd0b8d93f5e8bada180e77d3fe2f4ee21f02 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3850ddd9c027fdbeb795dedb7727093f9376fcdebb408372255d936c07c29182 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3850ddd9c027fdbeb795dedb7727093f9376fcdebb408372255d936c07c29182->enter($__internal_3850ddd9c027fdbeb795dedb7727093f9376fcdebb408372255d936c07c29182_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-minus-square.svg"));

        $__internal_c000b07fc1190c27c2caa4803fe50b43ae04ca2068a43acf56f27a2dafa3de8e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c000b07fc1190c27c2caa4803fe50b43ae04ca2068a43acf56f27a2dafa3de8e->enter($__internal_c000b07fc1190c27c2caa4803fe50b43ae04ca2068a43acf56f27a2dafa3de8e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-minus-square.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1408 960V832q0-26-19-45t-45-19H448q-26 0-45 19t-19 45v128q0 26 19 45t45 19h896q26 0 45-19t19-45zm256-544v960q0 119-84.5 203.5T1376 1664H416q-119 0-203.5-84.5T128 1376V416q0-119 84.5-203.5T416 128h960q119 0 203.5 84.5T1664 416z\"/></svg>
";
        
        $__internal_3850ddd9c027fdbeb795dedb7727093f9376fcdebb408372255d936c07c29182->leave($__internal_3850ddd9c027fdbeb795dedb7727093f9376fcdebb408372255d936c07c29182_prof);

        
        $__internal_c000b07fc1190c27c2caa4803fe50b43ae04ca2068a43acf56f27a2dafa3de8e->leave($__internal_c000b07fc1190c27c2caa4803fe50b43ae04ca2068a43acf56f27a2dafa3de8e_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/icon-minus-square.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1408 960V832q0-26-19-45t-45-19H448q-26 0-45 19t-19 45v128q0 26 19 45t45 19h896q26 0 45-19t19-45zm256-544v960q0 119-84.5 203.5T1376 1664H416q-119 0-203.5-84.5T128 1376V416q0-119 84.5-203.5T416 128h960q119 0 203.5 84.5T1664 416z\"/></svg>
", "@Twig/images/icon-minus-square.svg", "/home/david/Escritorio/test/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/images/icon-minus-square.svg");
    }
}
