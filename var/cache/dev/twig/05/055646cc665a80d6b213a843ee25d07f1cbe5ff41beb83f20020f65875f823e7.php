<?php

/* default/form.html.twig */
class __TwigTemplate_a0df0939a370fcb75550890cb662d87818972d50116a4aafaf0738fa35b2fb6f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "default/form.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_960eb9a6a0edbbde639cfb47d9c854511007a89394055ef1828ef60dc585262e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_960eb9a6a0edbbde639cfb47d9c854511007a89394055ef1828ef60dc585262e->enter($__internal_960eb9a6a0edbbde639cfb47d9c854511007a89394055ef1828ef60dc585262e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/form.html.twig"));

        $__internal_7f0c8e8555718eae4d1f7df76ebe13a25e7425d25391b717af3c1efd373d3e43 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7f0c8e8555718eae4d1f7df76ebe13a25e7425d25391b717af3c1efd373d3e43->enter($__internal_7f0c8e8555718eae4d1f7df76ebe13a25e7425d25391b717af3c1efd373d3e43_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/form.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_960eb9a6a0edbbde639cfb47d9c854511007a89394055ef1828ef60dc585262e->leave($__internal_960eb9a6a0edbbde639cfb47d9c854511007a89394055ef1828ef60dc585262e_prof);

        
        $__internal_7f0c8e8555718eae4d1f7df76ebe13a25e7425d25391b717af3c1efd373d3e43->leave($__internal_7f0c8e8555718eae4d1f7df76ebe13a25e7425d25391b717af3c1efd373d3e43_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_590059961a95f50cbd87c5d3ff6a85d42d86d3473a659a5d12115d6a3a7d1a39 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_590059961a95f50cbd87c5d3ff6a85d42d86d3473a659a5d12115d6a3a7d1a39->enter($__internal_590059961a95f50cbd87c5d3ff6a85d42d86d3473a659a5d12115d6a3a7d1a39_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_240b284740f8b65ff298b5b610adc16c54ea28bb8041e5aafafccc125c9b0ee6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_240b284740f8b65ff298b5b610adc16c54ea28bb8041e5aafafccc125c9b0ee6->enter($__internal_240b284740f8b65ff298b5b610adc16c54ea28bb8041e5aafafccc125c9b0ee6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h3> ";
        echo twig_escape_filter($this->env, ($context["title"] ?? $this->getContext($context, "title")), "html", null, true);
        echo " </h3>
    ";
        // line 5
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
    ";
        // line 6
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
    ";
        // line 7
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
";
        
        $__internal_240b284740f8b65ff298b5b610adc16c54ea28bb8041e5aafafccc125c9b0ee6->leave($__internal_240b284740f8b65ff298b5b610adc16c54ea28bb8041e5aafafccc125c9b0ee6_prof);

        
        $__internal_590059961a95f50cbd87c5d3ff6a85d42d86d3473a659a5d12115d6a3a7d1a39->leave($__internal_590059961a95f50cbd87c5d3ff6a85d42d86d3473a659a5d12115d6a3a7d1a39_prof);

    }

    public function getTemplateName()
    {
        return "default/form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  62 => 7,  58 => 6,  54 => 5,  49 => 4,  40 => 3,  11 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# app/Resources/views/default/form.html.twig #}
{% extends 'base.html.twig' %}
{% block body %}
    <h3> {{title}} </h3>
    {{ form_start(form) }}
    {{ form_widget(form) }}
    {{ form_end(form) }}
{% endblock %}



", "default/form.html.twig", "/home/david/Escritorio/test/app/Resources/views/default/form.html.twig");
    }
}
