<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_8036970b2f6c4eebcb26741f343431e46198547e170ecb11af2f9ef3d6502a5f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bd53765c8af4c29e94650ba7d14df8e5287b4620830c3d657e4bd7152c813016 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bd53765c8af4c29e94650ba7d14df8e5287b4620830c3d657e4bd7152c813016->enter($__internal_bd53765c8af4c29e94650ba7d14df8e5287b4620830c3d657e4bd7152c813016_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_61092822513d34f8e43be8143779155c6bea2792b7247076ce0368b244f64aeb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_61092822513d34f8e43be8143779155c6bea2792b7247076ce0368b244f64aeb->enter($__internal_61092822513d34f8e43be8143779155c6bea2792b7247076ce0368b244f64aeb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_bd53765c8af4c29e94650ba7d14df8e5287b4620830c3d657e4bd7152c813016->leave($__internal_bd53765c8af4c29e94650ba7d14df8e5287b4620830c3d657e4bd7152c813016_prof);

        
        $__internal_61092822513d34f8e43be8143779155c6bea2792b7247076ce0368b244f64aeb->leave($__internal_61092822513d34f8e43be8143779155c6bea2792b7247076ce0368b244f64aeb_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_c6eda640023120a92f2863a90ee1e77df5989bdc3f59d579212f3e9b86aa4beb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c6eda640023120a92f2863a90ee1e77df5989bdc3f59d579212f3e9b86aa4beb->enter($__internal_c6eda640023120a92f2863a90ee1e77df5989bdc3f59d579212f3e9b86aa4beb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_b238282b6ea285a46af5753c32942aff9ffbabd3a0ffb50958d01a7cf0ebc67c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b238282b6ea285a46af5753c32942aff9ffbabd3a0ffb50958d01a7cf0ebc67c->enter($__internal_b238282b6ea285a46af5753c32942aff9ffbabd3a0ffb50958d01a7cf0ebc67c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_b238282b6ea285a46af5753c32942aff9ffbabd3a0ffb50958d01a7cf0ebc67c->leave($__internal_b238282b6ea285a46af5753c32942aff9ffbabd3a0ffb50958d01a7cf0ebc67c_prof);

        
        $__internal_c6eda640023120a92f2863a90ee1e77df5989bdc3f59d579212f3e9b86aa4beb->leave($__internal_c6eda640023120a92f2863a90ee1e77df5989bdc3f59d579212f3e9b86aa4beb_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_81488ce93097275f8399262d237958a7385754fb8af7ea905cdc96adff7a6a03 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_81488ce93097275f8399262d237958a7385754fb8af7ea905cdc96adff7a6a03->enter($__internal_81488ce93097275f8399262d237958a7385754fb8af7ea905cdc96adff7a6a03_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_12c8fae4d1a983e7ff4803e9f408f327e362acb484f5a47e712c8b6c72121667 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_12c8fae4d1a983e7ff4803e9f408f327e362acb484f5a47e712c8b6c72121667->enter($__internal_12c8fae4d1a983e7ff4803e9f408f327e362acb484f5a47e712c8b6c72121667_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_12c8fae4d1a983e7ff4803e9f408f327e362acb484f5a47e712c8b6c72121667->leave($__internal_12c8fae4d1a983e7ff4803e9f408f327e362acb484f5a47e712c8b6c72121667_prof);

        
        $__internal_81488ce93097275f8399262d237958a7385754fb8af7ea905cdc96adff7a6a03->leave($__internal_81488ce93097275f8399262d237958a7385754fb8af7ea905cdc96adff7a6a03_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_6e11e3c281618fd070775deaec62d5ca53d44775c3bebf74a38e47ec80257f3c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6e11e3c281618fd070775deaec62d5ca53d44775c3bebf74a38e47ec80257f3c->enter($__internal_6e11e3c281618fd070775deaec62d5ca53d44775c3bebf74a38e47ec80257f3c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_3578b1ebfe1eb02022863fbad229d0bfd246205eea99640111e29892924096a2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3578b1ebfe1eb02022863fbad229d0bfd246205eea99640111e29892924096a2->enter($__internal_3578b1ebfe1eb02022863fbad229d0bfd246205eea99640111e29892924096a2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_3578b1ebfe1eb02022863fbad229d0bfd246205eea99640111e29892924096a2->leave($__internal_3578b1ebfe1eb02022863fbad229d0bfd246205eea99640111e29892924096a2_prof);

        
        $__internal_6e11e3c281618fd070775deaec62d5ca53d44775c3bebf74a38e47ec80257f3c->leave($__internal_6e11e3c281618fd070775deaec62d5ca53d44775c3bebf74a38e47ec80257f3c_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "/home/david/Escritorio/test/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.html.twig");
    }
}
