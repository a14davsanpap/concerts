<?php

// src/AppBundle/Entity/Concert.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="concert")
 */
class Concert
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $codi;

    /**
     * @ORM\Column(type="text", length=100)
     */
    private $nom;

    /**
     * @ORM\Column(type="text", length=255)
     */
    private $autor;

    /**
     * @ORM\Column(type="text", length=100)
     */
    private $nomgrup;

    /**
     * @ORM\Column(type="text", length=100)
     */
    private $data;

    /**
     * @ORM\Column(type="text", length=100)
     */
    private $ciutat;

    /**
     * @ORM\Column(type="text", length=100)
     */
    private $espai;

    /**
     * Get codi
     *
     * @return integer
     */
    public function getCodi()
    {
        return $this->codi;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Concert
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set autor
     *
     * @param string $autor
     *
     * @return Concert
     */
    public function setAutor($autor)
    {
        $this->autor = $autor;

        return $this;
    }

    /**
     * Get autor
     *
     * @return string
     */
    public function getAutor()
    {
        return $this->autor;
    }

    /**
     * Set nomgrup
     *
     * @param string $nomgrup
     *
     * @return Concert
     */
    public function setNomgrup($nomgrup)
    {
        $this->nomgrup = $nomgrup;

        return $this;
    }

    /**
     * Get nomgrup
     *
     * @return string
     */
    public function getNomgrup()
    {
        return $this->nomgrup;
    }

    /**
     * Set data
     *
     * @param string $data
     *
     * @return Concert
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set ciutat
     *
     * @param string $ciutat
     *
     * @return Concert
     */
    public function setCiutat($ciutat)
    {
        $this->ciutat = $ciutat;

        return $this;
    }

    /**
     * Get ciutat
     *
     * @return string
     */
    public function getCiutat()
    {
        return $this->ciutat;
    }

    /**
     * Set espai
     *
     * @param string $espai
     *
     * @return Concert
     */
    public function setEspai($espai)
    {
        $this->espai = $espai;

        return $this;
    }

    /**
     * Get espai
     *
     * @return string
     */
    public function getEspai()
    {
        return $this->espai;
    }
}
