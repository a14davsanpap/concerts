<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Concert;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ConcertController extends Controller
{

  /**
   * @Route("/insertConcert", name="insertConcert")
   */
  public function insertConcert(Request $request)
  {
      $concert = new Concert();

      $form = $this->createFormBuilder($concert)
          ->add('nom', TextType::class)
          ->add('autor', TextType::class)
          ->add('nomgrup', TextareaType::class)
          ->add('data', TextType::class)
          ->add('ciutat', TextType::class)
          ->add('espai', TextType::class)
          ->add('save', SubmitType::class, array('label' => 'Insereix'))
          ->getForm();

      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {
          $em = $this->getDoctrine()->getManager();
          $em->persist($concert);
          $em->flush();
          return $this->render('default/message.html.twig', array(
              'message' => 'Concert inserit: '. $concert->getcodi()
          ));
      }
      return $this->render('default/form.html.twig', array(
          'title' => 'Insereix Concert',
          'form' => $form->createView(),
      ));
  }



  /**
       * @Route("/removeConcert", name="removeConcert")
       */
      public function removeProductAction(Request $request)
      {
          $concert = new Concert();

          $form = $this->createFormBuilder($concert)
              ->add('nom', TextType::class)
              ->add('save', SubmitType::class, array('label' => 'Eliminar'))
              ->getForm();

          $form->handleRequest($request);

          if ($form->isSubmitted() && $form->isValid()) {
              $em = $this->getDoctrine()->getManager();
              $concert = $em->getRepository('AppBundle:Concert')
                        ->findOneBynom($concert->getnom());
              if (!$concert) {
                  return $this->render('default/message.html.twig', array(
                      'message' => 'No products found for name '. $concert->getnom()));
              }
              $id = $concert->getnom();
              $em->remove($concert);
              $em->flush();
              return $this->render('default/message.html.twig', array(
                  'message' => 'Concert eliminat '. $id
              ));

          }
          return $this->render('default/form.html.twig', array(
              'title' => 'Remove Concert',
              'form' => $form->createView(),
          ));
      }


      /**
           * @Route("/selectConcerts", name="selectConcerts")
           */
          public function selectConcerts()
          {
              $concert = $this->getDoctrine()
                  ->getRepository('AppBundle:Concert')
                  ->findAll();

              if (count($concert)==0) {
                  return $this->render('default/message.html.twig', array(
                      'message' => 'No customers found'));
              }
              return $this->render('concert/content.html.twig', array(
                  'concert' => $concert));
          }




          /**
              * @Route("/selecionarConcert", name="selecionarConcert")
              */
             public function selecionarConcertAction(Request $request)
             {
                 $concert = new Concert();

                 $form = $this->createFormBuilder($concert)
                     ->add('nom', TextType::class)
                     ->add('save', SubmitType::class, array('label' => 'Cercar'))
                     ->getForm();

                 $form->handleRequest($request);

                 if ($form->isSubmitted() && $form->isValid()) {
                     $concert = $this->getDoctrine()
                         ->getRepository('AppBundle:Concert')
                         ->findBynom($concert->getnom());
                     if (count($concert)==0) {
                         return $this->render('default/message.html.twig', array(
                             'message' => 'No customers found'));
                     }
                     return $this->render('concert/content.html.twig', array(
                         'concert' => $concert));
                 }
                 return $this->render('default/form.html.twig', array(
                     'title' => 'Selecionar Concert',
                     'form' => $form->createView(),
                 ));
             }




}
